# Kanban notifications

This piece of software will integrate Devscola's kanban (trello) and Devscola's chat (mattermost). These notifications will include from card creation, to comments added, and card movements.

## Development

### Stack

- Python
- Flask
- Docker

### Requirements

- docker
- docker-compose

### Up & running

> $ docker-compose up

An API will be accessible at: [localhost:5000](http://localhost:5000).

Use Ctrl+C to stop it, and run this command to ensure your system is clean:

> $ docker-compose down

### Folder structure

- `API` is contained in `api` folder

### How to use Test

- docker-compose run --rm api pipenv run mamba --format=documentation