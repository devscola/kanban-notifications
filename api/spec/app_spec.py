from mamba import description, context, it
from expects import expect, equal, contain
import json
import requests

with description('App') as self:
  with it('returns 200 when reception is notified'):
    url = 'http://api:5000/notify'

    response = requests.post(url)

    expect(response.status_code).to(equal(200))

  with it('connects to mattermost when notification is received'):
    url = 'http://api:5000/notify'
    document = {
      'action': {
        'type': 'createdCard'
      }
    }

    requests.post(url, json.dumps(document))

    expected_message = {
      'text': 'Tarjeta creada'
    }
    received_messages = get_mattermost_messages()
    expect(received_messages).to(contain(expected_message))

def get_mattermost_messages():
  response = requests.get('http://mattermost:5001/')
  return json.loads(response.text)
