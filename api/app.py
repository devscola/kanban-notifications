from flask import Flask
import requests

app = Flask(__name__)

mattermost_url = 'http://mattermost:5001/'

@app.route('/notify', methods=['POST'])
def notify():
    message = {
      'text': 'Tarjeta creada'
    }
    post_json(mattermost_url, message)
    return 'ok'

def post_json(url, data):
    return requests.post(url, json=data)
