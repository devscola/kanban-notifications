from flask import Flask
from flask import request
from flask import jsonify

app = Flask(__name__)
notifications = []

@app.route('/')
def fetch():
    response = jsonify(notifications)
    return response

@app.route('/', methods=['POST'])
def notify():
    message = request.get_json()
    notifications.append(message)
    return 'Ok'